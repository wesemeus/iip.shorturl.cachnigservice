﻿using IIP.ShortUrl.CachingService.Data.Repositories.Interfaces;
using Microsoft.Extensions.Caching.Distributed;

namespace IIP.ShortUrl.CachingService.Data.Repositories;

public sealed class UrlCacheRepository: IUrlCacheRepository
{
    private const int ExpirationTimeInMinutes = 2;
    private readonly IDistributedCache _cache;

    public UrlCacheRepository(IDistributedCache cache)
    {
        _cache = cache;
    }

    public async Task<bool> AddToCacheAsync(string key, string value)
    {
        try
        {
            await _cache.SetStringAsync(key, value, new DistributedCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(ExpirationTimeInMinutes)
            });
            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public async Task<string?> GetFromCacheAsync(string key)
    {
        return await _cache.GetStringAsync(key);
    }
}