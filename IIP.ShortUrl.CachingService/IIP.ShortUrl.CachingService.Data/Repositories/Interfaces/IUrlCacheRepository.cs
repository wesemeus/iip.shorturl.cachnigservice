﻿namespace IIP.ShortUrl.CachingService.Data.Repositories.Interfaces;

public interface IUrlCacheRepository
{
    Task<bool> AddToCacheAsync(string key, string value);

    Task<string?> GetFromCacheAsync(string key);
}