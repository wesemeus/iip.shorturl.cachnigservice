﻿using IIP.Dto.Producers;
using IIP.Dto.Requests.GetUrlByHash;
using IIP.Dto.Requests.GetUrlFromCache;
using IIP.Dto.Requests.SaveUrlToCache;
using IIP.ShortUrl.CachingService.Data.Repositories.Interfaces;
using IIP.ShortUrl.CachingService.Logic.Services.Interfaces;

namespace IIP.ShortUrl.CachingService.Logic.Services;

public sealed class CachingService: ICachingService
{
    private readonly IUrlCacheRepository _repository;
    private readonly IGetUrlByHashProducer _getUrlByHashProducer;

    public CachingService(IUrlCacheRepository repository, 
        IGetUrlByHashProducer getUrlByHashProducer)
    {
        _repository = repository;
        _getUrlByHashProducer = getUrlByHashProducer;
    }

    public async Task<GetUrlFromCacheResponse> GetUrlFromCacheAsync(GetUrlFromCacheRequest request)
    {
        var result = await _repository.GetFromCacheAsync(request.Hash!);
        if (result is null)
        {
            var shortUrl = await _getUrlByHashProducer.RequestAsync(new GetUrlByHashRequest()
            {
                Hash = request.Hash
            });
            if (shortUrl.Url is not null)
            {
                result = shortUrl.Url;
                await _repository.AddToCacheAsync(request.Hash!, shortUrl.Url);
            }
        }
        return new GetUrlFromCacheResponse()
        {
            Url = result
        };
    }

    public async Task<SaveUrlToCacheResponse> SaveUrlToCacheAsync(SaveUrlToCacheRequest request)
    {
        var result = await _repository.AddToCacheAsync(request.Hash!, request.Url!);
        return new SaveUrlToCacheResponse()
        {
            IsSuccess = result
        };
    }
}