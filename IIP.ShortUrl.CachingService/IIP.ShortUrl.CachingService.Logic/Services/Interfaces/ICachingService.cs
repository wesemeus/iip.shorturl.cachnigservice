﻿using IIP.Dto.Requests.GetUrlFromCache;
using IIP.Dto.Requests.SaveUrlToCache;

namespace IIP.ShortUrl.CachingService.Logic.Services.Interfaces;

public interface ICachingService
{
    Task<GetUrlFromCacheResponse> GetUrlFromCacheAsync(GetUrlFromCacheRequest request);

    Task<SaveUrlToCacheResponse> SaveUrlToCacheAsync(SaveUrlToCacheRequest request);
}