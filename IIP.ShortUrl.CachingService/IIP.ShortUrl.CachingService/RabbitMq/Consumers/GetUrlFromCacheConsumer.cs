﻿using IIP.Dto.Requests.GetUrlFromCache;
using IIP.ShortUrl.CachingService.Logic.Services.Interfaces;
using MassTransit;

namespace IIP.ShortUrl.CachingService.RabbitMq.Consumers;

public sealed class GetUrlFromCacheConsumer: IConsumer<GetUrlFromCacheRequest>
{
    private readonly ICachingService _cachingService;

    public GetUrlFromCacheConsumer(ICachingService cachingService)
    {
        _cachingService = cachingService;
    }

    public async Task Consume(ConsumeContext<GetUrlFromCacheRequest> context)
    {
        var result = await _cachingService.GetUrlFromCacheAsync(context.Message);
        await context.RespondAsync(result);
    }
}