﻿using IIP.Dto.Requests.SaveUrlToCache;
using IIP.ShortUrl.CachingService.Logic.Services.Interfaces;
using MassTransit;

namespace IIP.ShortUrl.CachingService.RabbitMq.Consumers;

public sealed class SaveUrlToCacheConsumer: IConsumer<SaveUrlToCacheRequest>
{
    private readonly ICachingService _cachingService;

    public SaveUrlToCacheConsumer(ICachingService cachingService)
    {
        _cachingService = cachingService;
    }

    public async Task Consume(ConsumeContext<SaveUrlToCacheRequest> context)
    {
        var result = await _cachingService.SaveUrlToCacheAsync(context.Message);
        await context.RespondAsync(result);
    }
}