using IIP.Dto.Producers;
using IIP.ShortUrl.CachingService.Data.Repositories;
using IIP.ShortUrl.CachingService.Data.Repositories.Interfaces;
using IIP.ShortUrl.CachingService.Logic.Services;
using IIP.ShortUrl.CachingService.Logic.Services.Interfaces;
using IIP.ShortUrl.CachingService.RabbitMq.Producers.GetUrlByHash;
using MassTransit;
using Prometheus;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<ICachingService, CachingService>();
builder.Services.AddScoped<IUrlCacheRepository, UrlCacheRepository>();
builder.Services.AddScoped<IGetUrlByHashProducer, GetUrlByHashProducer>();

builder.Services.AddStackExchangeRedisCache(options => {
    options.Configuration = "localhost";
    options.InstanceName = "local";
});

builder.Services.AddMassTransit(cfg =>
{
    cfg.SetKebabCaseEndpointNameFormatter();
    cfg.AddConsumers(typeof(Program).Assembly);
    cfg.UsingRabbitMq((ctx, c) =>
    {
        c.Host("localhost", "/", h => {
            h.Username("guest");
            h.Password("guest");
            h.UseCluster(cluster =>
            {
                cluster.Node("localhost:5672");
                cluster.Node("localhost:5673");
            });
        });
                
        c.ConfigureEndpoints(ctx);
    });
});

var app = builder.Build();

app.UseHttpMetrics();
app.MapMetrics();

app.Run();